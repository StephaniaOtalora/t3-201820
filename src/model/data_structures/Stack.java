package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack <T extends Object> implements IStack<T> {
	private int size;
	private Node head;

	public Stack() 
	{
		// TODO Auto-generated constructor stub
		size = 0;
		head = null;
	}
	@Override
	public Iterator<T> iterator() 
	{
		Iterador iter =new Iterador<T>(head);
		return iter;
	}

	@Override
	public boolean isEmpty()
	{
		boolean resp=false;
		if(size==0)
		{
			resp = true;
		}
		return resp;
	}

	@Override
	public int size() 
	{
		return size;
	}

	@Override
	public void push(T t)
	{

		boolean check = false;
		Node ne = new Node<Object>(null, null, t);
		if(isEmpty())
		{
			head = ne;
			check = true;
		}
		else
		{
			head.changePrev(ne);
			ne.changeNext(head);
			head = ne;
			check = true;
		}

		if(check) size ++;

	}


	@Override
	public T pop() 
	{
		T var =null;
		if(isEmpty())
		{
			throw new NoSuchElementException("La pila esta vacia");
		}
		else if(head.getNext()!=null)
		{
			var=(T) head.getElement();
			head=head.getNext();
			head.changePrev(null);
			size--;
		}
		else
		{
			var=(T) head.getElement();
			head=null;
			size--;
		}

		return (T)var;
	}
}



