/**
 * 
 */
package model.data_structures;

import java.util.Iterator;


public class Iterador<T> implements Iterator<T> {

	private Node actual;

	public Iterador(Node first)
	{
		actual = first;
	}
	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		boolean check = false;
		if(actual!=null) check = true;
		return check;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public T next() {
		// TODO Auto-generated method stub
		T ans = (T) actual.getElement();
		actual = actual.getNext();
		return ans;
	}

}
