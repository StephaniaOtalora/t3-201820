package model.data_structures;

import java.util.Iterator;

public class Queue <T extends Object> implements IQueue<T> 
{
	private int size;
	
	private Node first;
	
	private Node last;
	
	
	public Queue() {
		// TODO Auto-generated constructor stub
		size = 0;
		first = null;
		last = null;
		
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterador<T> iter = new Iterador<T>(first);
		return iter;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		boolean empty = true;
		if(size!= 0) empty = false;
		return empty;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		boolean check = false;
		Node ne = new Node<Object>(null, null, t);
		if(isEmpty())
		{
			first = ne;
			last = first;
			check = true;
		}
		else
		{
			last.changeNext(ne);
			ne.changePrev(last);
			last = ne;
			check = true;
		}
		
		if(check) size ++;

	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		boolean check = false;
		Node de = null;
		if(first.getNext()!=null)
		{
			de = first;
			first = first.getNext();
			de.changeNext(null);
			first.changePrev(null);
			check = true;	
		}
		else if( first.getNext()==null)
		{
			de = first;
			check = true;	
		}
		if(check) size --;
		return (T) de.getElement();
	}

}
