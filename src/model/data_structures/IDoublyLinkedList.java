package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	//TODO Agregar API de DoublyLinkedList

	int getSize();

	boolean add(T pT);

	boolean addAtEnd(T pT);

	boolean addAtK(int k, T pT);

	T getCurrentElement();

	T getElement(int k);	

	boolean delete(T pT);

	boolean deleteAtk(int k);

	Node getNext();

	Node getPrevious();

}
