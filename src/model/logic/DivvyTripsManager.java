package model.logic;

import java.io.FileReader;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Iterador;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager 
{

	private DoublyLinkedList<VOStation> stations;
	private Stack<VOTrip> tripsStack;
	private Queue<VOTrip> tripsQueue;

	public DivvyTripsManager()
	{
		stations = new DoublyLinkedList<VOStation>();
		tripsStack = new Stack<VOTrip>();
		tripsQueue = new Queue<VOTrip>();

	}

	public void loadStations (String stationsFile) 
	{
		try
		{
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] nextLine = reader.readNext();
			while (nextLine != null) 
			{
				if(!nextLine[0].equals("id"))
				{
					int pId = Integer.parseInt(nextLine[0]);
					double pLatitude = Double.parseDouble(nextLine[3]);
					double pLongitud = Double.parseDouble(nextLine[4]);
					int pCapacity = Integer.parseInt(nextLine[5]);
					VOStation ne = new VOStation(pId, nextLine[1], nextLine[2], pLatitude, pLongitud, pCapacity, nextLine[6]);
					stations.add(ne);
				}
				nextLine = reader.readNext();
			}
			reader.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	public void loadTrips (String tripsFile) 
	{
		try
		{
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] nextLine = reader.readNext();
			while (nextLine != null) 
			{
				if(!nextLine[0].equalsIgnoreCase("trip_id"))
				{
					int pId = Integer.parseInt(nextLine[0]);
					int pBikeId = Integer.parseInt(nextLine[3]);
					double pdur = Double.parseDouble(nextLine[4]);
					int pstFrId = Integer.parseInt(nextLine[5]);
					int pstToId = Integer.parseInt(nextLine[7]);
					VOTrip ne = new VOTrip(pId, nextLine[1], nextLine[2], pBikeId, pdur, nextLine[6], pstFrId, nextLine[8], pstToId, nextLine[9], nextLine[10], nextLine[11]);
					tripsStack.push(ne);
					tripsQueue.enqueue(ne);
				}
				nextLine = reader.readNext();
			}
			reader.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	@Override
	public DoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		// TODO Auto-generated method stub

		DoublyLinkedList<String> lastNstations = new DoublyLinkedList<String>();

		Iterador<VOTrip> rep = (Iterador<VOTrip>) tripsQueue.iterator();
		Queue<VOTrip> copy = tripsQueue; 

		while(rep.hasNext() && n>0)
		{
			VOTrip elem = copy.dequeue();
			if(elem.getBikeID() == bicycleId)
			{
				lastNstations.add(elem.getToStation());
				n--;
			}
		}

		return lastNstations;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n)
	{

		VOTrip resp=null;

		Iterador<VOTrip> iterator  = (Iterador<VOTrip>) tripsQueue.iterator();
		Stack<VOTrip> copy = tripsStack; 

		while(iterator.hasNext() && n>0)
		{
			VOTrip elem = copy.pop();
			if(elem.getToStationId() == stationID)
			{
				resp=elem;
				n--;
			}
		}

		return resp;
	}


}
