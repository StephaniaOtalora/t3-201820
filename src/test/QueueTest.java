package test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import model.data_structures.Queue;

public class QueueTest 
{
	private Queue<Object> queue;

	public void setUpEscenario1()
	{
		queue = new Queue<Object>();
	}

	@Test
	public void testDatos( )
	{
		setUpEscenario1();
		assertEquals( "El tama�o es incorrecto", 0, queue.size() );
	}

	@Test
	public void testIsEmpty( )
	{
		setUpEscenario1();
		assertEquals( "Deber�a estar vac�a la cola", true, queue.isEmpty() );

		Object ele = new Object();
		Object ele1 = new Object();

		queue.enqueue(ele);
		queue.enqueue(ele1);

		assertEquals("No deber�a estar vac�a la cola",false , queue.isEmpty());
	}

	@Test
	public void testEnqueue( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();

		queue.enqueue(ele);
		assertEquals("No se agrego el elemento", 1, queue.size());

		queue.enqueue(ele1);
		assertEquals("No se agrego el elemento", 2, queue.size());

		queue.enqueue(ele2);
		assertEquals("No se agrego el elemento", 3, queue.size());
	}

	@Test
	public void testDequeue( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();

		queue.enqueue(ele);
		queue.enqueue(ele1);
		queue.enqueue(ele2);
		assertEquals("No se agrego el elemento", 3, queue.size());

		queue.dequeue();
		assertEquals("No se elimino el elemento", 2, queue.size());

		assertEquals("No se elimina el elemento deseado", ele1, queue.dequeue());

	}




}
