package test;

import static org.junit.Assert.assertEquals;

import java.util.Stack;

import org.junit.Test;


public class StackTest 
{
	private Stack<Object> stack;

	public void setUpEscenario1()
	{
		stack = new Stack<Object>();
	}

	@Test
	public void testDatos( )
	{
		setUpEscenario1();
		assertEquals( "La pila deberia estar vacia", 0, stack.size() );
	}

	@Test
	public void testIsEmpty( )
	{
		setUpEscenario1();
		//pila vacia
		assertEquals( "La pila deberia estar vacia", true, stack.isEmpty() );

		Object p1 = new Object();
		Object p2 = new Object();
		Object p3 = new Object();

		stack.push(p1);
		stack.push(p2);
		stack.push(p3);
		//pila con 3 elementos
		assertEquals("Deberia haber datos en la pila",false , stack.isEmpty());
	}

	@Test
	public void testPush( )
	{
		setUpEscenario1();
		Object p1 = new Object();
		Object p2 = new Object();
		//agrega primer elemento
		stack.push(p1);
		assertEquals("No se agrega p1", 1, stack.size());
		//agrega mas elementos
		stack.push(p2);
		assertEquals("No se agrega p2", 2, stack.size());
	}

	@Test
	public void testPop( )
	{
		setUpEscenario1();
		Object p1 = new Object();
		Object p2 = new Object();
		Object p3= new Object();

		stack.push(p1);
		stack.push(p2);
		stack.push(p3);
		stack.pop();
		assertEquals("No se elimino el elemento", 2, stack.size());

		assertEquals("No se elimina el elemento deseado", p2 , stack.pop());

	}




}

